const path = require('path');

module.exports = {
    mode: 'production',
    entry: './out/work-main.js',
    output: {
        path: path.resolve(__dirname, 'out', 'bundle'),
        filename: 'work.bundle.js'
    }
};
