import BigNumber from 'bignumber.js';

import { assertBigNumberArraySameElements } from './asserts';
import { nCCSequences } from './collatz-sim';

console.log("Testing...");
const toBigNumberArr = (a) => a.map((n) => new BigNumber(n));
assertBigNumberArraySameElements(nCCSequences(3).nodes, toBigNumberArr([4]));
assertBigNumberArraySameElements(nCCSequences(4).nodes, toBigNumberArr([8]));
assertBigNumberArraySameElements(nCCSequences(5).nodes, toBigNumberArr([16]));
assertBigNumberArraySameElements(nCCSequences(6).nodes, toBigNumberArr([32, 5]));
assertBigNumberArraySameElements(nCCSequences(7).nodes, toBigNumberArr([10, 64]));
assertBigNumberArraySameElements(nCCSequences(12).nodes, toBigNumberArr([48, 52, 53, 320, 336, 340, 341, 2048]));
assertBigNumberArraySameElements(nCCSequences(13, new BigNumber(10), 7).nodes, nCCSequences(13, new BigNumber(3), 8).nodes.concat(nCCSequences(13, new BigNumber(20), 8).nodes));
assertBigNumberArraySameElements(nCCSequences(12, new BigNumber(128), 8).nodes, nCCSequences(12, new BigNumber(256), 9).nodes);
assertBigNumberArraySameElements(nCCSequences(12, new BigNumber(128), 8).nodes, nCCSequences(12, new BigNumber(85), 10).nodes.concat(nCCSequences(12, new BigNumber(512), 10).nodes));
console.log("Nice!");

const n = 50;
console.log(`Finding sequences of length ${n}`);

const { nodes, length, totalNodes } = nCCSequences(n);
console.log("Sequences:", length);
console.log("Points searched:", totalNodes);
console.log("Sample node:", nodes[0].toString());
