import * as dcpClient from 'dcp-client';
import * as fs from 'fs';
import { URL } from 'url';

import { nCCSequences } from './collatz-sim';

const SCHEDULER_URLS = {
    PRODUCTION: new URL('https://scheduler.distributed.computer'),
    STAGING:    new URL('http://scheduler.staging.office.kingsds.network'),
    DEVSERVER:  new URL('http://scheduler.devserver.office.kingsds.network'),
}

const buildWorkFn = () => {
    const workBundle = fs.readFileSync('./out/bundle/work.bundle.js', 'utf8');
    return `(...args) => {
        ${workBundle}
        return global.workFn(...args);
    } `;
}

const main = async () => {
    await dcpClient.init(SCHEDULER_URLS.DEVSERVER, true);
    const compute = require('dcp/compute');

    const startingDepth = 10; //25
    const depth = 50; //85
    const data = nCCSequences(startingDepth).nodes.map(n => [n, startingDepth]);
    // const data = [];
    // for (let i = 4; i + 1 < depth; i += 2) {
    //     const startNode = (new BigNumber(2)).pow(i).minus(1).div(3); // (2 ** i - 1) / 3
    //     const startIndex = i + 2;
    //     data.push([startNode, startIndex]);
    // }

    console.log(`Deploying job with ${data.length} slices... (startingDepth: ${startingDepth}, target: ${depth})`);
    const job = compute.for(data, buildWorkFn(), [depth]);
    Object.assign(global, { job });

    job.public.name = 'Collatz Conjecture Sequences';
    job.on('accepted', () => console.log("Job accepted", job.id));
    job.on('status', console.log);
    job.work.on('console', console.log);
    job.work.on('uncaughtException', console.error);

    const results = await job.exec();
    const arr = Array.from(results);
    const total = results.reduce((t, v) => t + v, 0); /* start at one when calculating off powers-of-2 */
    console.log("Done!", total, arr);
}

main();
