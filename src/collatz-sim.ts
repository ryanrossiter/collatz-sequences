import BigNumber from 'bignumber.js';

declare const progress: Function;

export const cc = (n : number) : number => {
    let c = 0;

    while (n !== 1) {
        if (n % 2 === 0) n /= 2;
        else n = n * 3 + 1;
        c++;
    }

    return c;
}

export const nCCSequences = (n : number, start=new BigNumber(1), startIndex=1) => {
    let nodes = [start];
    let totalNodes = 0;

    let nextNodes : BigNumber[] = [];
    for (let c = startIndex; c < n; c++) {
        if (typeof progress === 'function') progress((c - startIndex) / (n - startIndex));
        totalNodes += nodes.length;
        while (nodes.length) {
            if (nodes.length % 50 === 0 && typeof progress === 'function') progress();
            let node = nodes.pop();

            nextNodes.push(node.times(2)); 
            if (node.mod(6).eq(4) && !node.eq(4)) {
                nextNodes.push(node.minus(1).div(3));
            }
        }

        nodes = nextNodes;
        nextNodes = [];
    }

    return {
        nodes,
        length: nodes.length,
        totalNodes,
    };
}
