import BigNumber from 'bignumber.js';

import { nCCSequences } from './collatz-sim';

declare const progress: Function;

Object.assign(global, {
    workFn([start, startIndex] : [any, number], depth) {
        progress();
        start = new BigNumber(start);
        console.log("Starting", depth, start.toString(), startIndex);
        const result = nCCSequences(depth, start, startIndex);
        return result.length;
    }
});
