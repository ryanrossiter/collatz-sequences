import BigNumber from "bignumber.js";

export const assert = (b : boolean, msg : string) => {
    if (!b) throw new Error(msg);
}

export const assertBigNumberArraySameElements = (a : BigNumber[], b : BigNumber[]) => {
    try {
        assert(a.length === b.length, "Array lengths not equal");
        a.forEach((ae, i) => assert(!!b.find(be => be.eq(ae)), `Array b doesn't contain ${ae}`));
    } catch (error) {
        console.log("A", a);
        console.log("B", b);
        throw error;
    }
}

export const assertThrows = (f : Function, ...args) => {
    let threw = false;
    try {
        f(...args);
    } catch (e) {
        threw = true;
    }

    if (!threw) {
        throw new Error("Function didn't throw");
    }
}

// assertArraySameElements([1,2,3,4], [4,3,2,1]);
// assertThrows(assertArraySameElements, [1,2,3,4], [4,3,2,1,5]);
// assertThrows(assertArraySameElements, [100,1,5], [412,1,5]);